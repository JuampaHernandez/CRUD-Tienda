package com.dao;

import com.conexion.Conexion;
import com.modelos.Venta;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 * Nombre de la clase: DaoVenta
 * Version: 1.0
 * Fecha: 2/8/2018
 * Copyright: Juan Pablo Elias
 * @author Juan Pablo Elias Hernández
 */
public class DaoVenta extends Conexion
{
    public List all()
    {
        List departamentos = new ArrayList();
        ResultSet res;
        try
        {
            conectar();
            String sql = "SELECT * FROM ventas;";
            PreparedStatement pre = getConn().prepareStatement(sql);
            res = pre.executeQuery();
            while (res.next())
            {   
                Venta ven = new Venta();
                ven.setId(res.getInt(1));
                ven.setProducto(res.getString(2));
                ven.setPrecio(res.getDouble(3));
                ven.setCantidad(res.getInt(4));
                ven.setTotal(res.getDouble(5));
                departamentos.add(ven);
            }
        }
        catch (Exception e)
        {
            
        }
        return departamentos;
    }
    
    public void add(Venta ven)
    {
        try
        {
            conectar();
            String sql = "INSERT INTO ventas VALUES (null, ?, ?, ?, ?);";
            PreparedStatement pre = getConn().prepareStatement(sql);
            pre.setString(1, ven.getProducto());
            pre.setDouble(2, ven.getPrecio());
            pre.setInt(3, ven.getCantidad());
            pre.setDouble(4, ven.getTotal());
            pre.executeUpdate();
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "Error al agregar: "
                    + e.toString(), "Error", 0);
        }
        finally
        {
            desconectar();
        }
    }
    
    public void edit(Venta ven)
    {
        try
        {
            conectar();
            String sql = "UPDATE ventas SET producto = ?, precio = ?, cantidad = ?, total = ? WHERE id = ?;";
            PreparedStatement pre = getConn().prepareStatement(sql);
            pre.setString(1, ven.getProducto());
            pre.setDouble(2, ven.getPrecio());
            pre.setInt(3, ven.getCantidad());
            pre.setDouble(4, ven.getTotal());
            pre.setInt(5, ven.getId());
            pre.executeUpdate();
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "Error al editar: "
                    + e.toString(), "Error", 0);
        }
        finally
        {
            desconectar();
        }
    }
    
    public void delete(Venta ven)
    {
        try
        {
            conectar();
            String sql = "DELETE FROM ventas WHERE id = ?;";
            PreparedStatement pre = getConn().prepareStatement(sql);
            pre.setInt(1, ven.getId());
            pre.executeUpdate();
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "Error al eliminar: "
                    + e.toString(), "Error", 0);
        }
        finally
        {
            desconectar();
        }
    }
}
