CREATE DATABASE IF NOT EXISTS tienda;
USE tienda;

CREATE TABLE IF NOT EXISTS ventas
(
	id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    producto VARCHAR(50) NOT NULL,
    precio DOUBLE NOT NULL,
    cantidad INT NOT NULL,
    total DOUBLE NOT NULL
);

INSERT INTO ventas VALUES (null, 'Prod 1', 1.25, 3, 3.75);
INSERT INTO ventas VALUES (null, 'Prod 2', 2, 4, 8);
INSERT INTO ventas VALUES (null, 'Prod 3', 1.5, 4, 6);

SELECT * FROM ventas;